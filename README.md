# Sequera VR

## Credits

* [sound](sound.ogg): Derived from ["everyday (06-04-14) massive / inverted proto"](https://soundcloud.com/standingwave/everyday-06-04-14-massive-inverted-proto), by [standingwave](https://soundcloud.com/standingwave). License: CC by-sa.